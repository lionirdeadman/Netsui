# Netsui

A GTK Anime list manager

## Goals

- Easily and beautifully manage anime lists
- Support multiple account and multiple services

## Credits
- [GTK-Rust-Template] (https://gitlab.gnome.org/bilelmoussaoui/netsui)
- [Podcasts](https://gitlab.gnome.org/World/podcasts)
- [Shortwave](https://gitlab.gnome.org/World/Shortwave)

